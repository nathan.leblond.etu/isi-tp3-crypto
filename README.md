TP3 - ISI - Cryptographie
===

# Leblond Nathan - M1 Machine Learning

### Remarque :
Vous pouvez initialiser chaque environnement en lançant le script ./init.sh.  
Cela créera les clefs et les mdp nécessaires à stocker  

## Partie 1: Responsabilité partagée
### Question 1: 

  - Ce que je sais: Chaque responsable doit connaître son mot de passe
  - Ce que j'ai: Chaque responsable doit être en possession de sa clef usb

Procédure:
  1. Le responsable A (resp. B) rentre son mot de passe : mdpA (resp. mdpB)
  2. mdpA (resp. mdpB) est "hashé" à partir d'une clef en utilisant aes-128-cbc et fourni hashA1 (resp. hashB1)
  3. hashA1 (resp. hashB1) est utilisé pour décrypter le contenu de la clef usbA (resp. usbB) et fourni hashA2 (resp. hashB2)
  4. Une fonction de (hashA2,hashB2) fournit la clef permettant de decrypter le fichier cible


### Question 2:
  Cf. dossier Q2

## Partie 2: Délégation de droit
### Question 3:
  On génère une clef a partir de /dev/random, cette clef "FINALE" sera la clef de décryptage/encryptage du fichier bancaire.  
  Pour que les 4 combinaisons d'acteurs puissent avoir des actions sur un même fichier encrypté par une clef unique, on ne pas pouvoir se passer de stocker une partie de l'information.  
  On appelle A1 le responsable A, A2 le suppléant A et de la même manière pour les B.  
  Ainsi, on adopte la procédure suivante :  

  1. Les responsables et leur suppléants fournissent leur mot de passe (on veut que les mots de passe des suppléants soient différents de celui de leur responsable associé)  
  2. On génère les clefs propres à chaque acteur
  3. On crée 4 versions cryptées de la clef "FINALE" à partir de chacune des combinaisons des clefs des acteurs : A1B1, A1B2, A2B1, A2B2  
  4. On stock les 4 versions cryptées, on supprime la version claire de clef "FINALE"
  5. On encrypte chaque clef par le mot de passe de l'acteur associé puis on le stocke sur sa clef usb
  6. Lors de l'ajout/suppression de paire :  
    i. On s'assure des acteurs en présence
    ii. On récupère leur mots de passe avec lesquels on décrypte les clefs stockées sur leur clef
    iii. On fait le XOR de leur clef respective decryptée afin de décrypter leur version de la clef finale
    iv. On utilise la clef finale pour decrypter le fichier bancaire et l'on stocke la clef finale jusqu'a la réencryption pour faciliter les manipulations
    v. On réalise les actions souhaitées
    vi. On réencrypte le fichier en se servant de la clef qui a été stocké puis on supprime la clef et le fichier en clair
### Question 4:
  Cf. dossier Q4 

## Partie 3: Répudiation
### Question 5:
  On compte sur le fait qu'un "bad decrypt" se produit chaque fois qu'on tente de décoder la clef d'un acteur avec un mauvais mot de passe.
  Ainsi, ici, pour répudier un utilisateur, les 3 autres doivent montrer qu'ils sont en mesure de décoder leur clef à l'aide leur mot de passe. Si les autres utilisateurs y parviennent, alors on se contente de supprimer le contenu de la clef usb de l'utilisateur à répudier ainsi que toutes les versions encryptées de la clef "FINALE" qui utilisaient ses informations. 
### Question 6:
  Cf. dossier Q6 



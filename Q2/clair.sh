#!/bin/bash

echo "DESENCRYPTION DE LA MAP BANCAIRE, SOYEZ PRUDENT"

echo "Mot de passe du responsable A (mdpA): "
read -s mdpA
MDPA=$(echo $mdpA | hexdump -ve '1/1 "%02X"')
CLEFA=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPA -iv 0 -in usbA/clefA )

echo "Mot de passe du responsable B (mdpB): "
read -s mdpB
MDPB=$(echo $mdpB | hexdump -ve '1/1 "%02X"')
CLEFB=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPB -iv 0 -in usbB/clefB )
CLEF=$((0x$CLEFA ^ 0x$CLEFB)) 

echo $CLEF >> clef

openssl enc -d -aes-128-cbc -K $(echo $CLEF | hexdump -ve '1/1 "%02X"') -iv 0 -in mapBancaire.enc -out mapBancaire 

#!/bin/bash

PWD=$(pwd)
ALGO=16
echo "Initialisation des clefs usbA et usbB"

# On initialise les dossiers/fichiers nécessaires
mkdir usb{A,B}
touch mapBancaire

#On génère des clefs aléatoire à partir des carac. physiques de la machine à l'initialisation
CLEFA=$(dd if=/dev/random bs=$ALGO count=1 &> /dev/null | hexdump -ve '1/1 "%02X"')
CLEFB=$(dd if=/dev/random bs=$ALGO count=1 &> /dev/null | hexdump -ve '1/1 "%02X"')

# On forme la clef finale qui servira pour le fichier à protéger, on l'encode avec puis on supprime la version claire
CLEF=$((0x$CLEFA ^ 0x$CLEFB))

openssl enc -aes-128-cbc -pbkdf2 -K $(echo $CLEF | hexdump -ve '1/1 "%02X"')  -in mapBancaire -iv 0 -out mapBancaire.enc &> /dev/null 
rm mapBancaire

#On génère les clefs usb qui permettront de reformer la clef finale 
echo "Entrez le mot de passe du responsable A (mdpA): "
read -s mdpA
MDPA=$(echo $mdpA | hexdump -ve '1/1 "%02X"')
echo $CLEFA | (openssl enc -aes-128-cbc -pbkdf2 -K $MDPA -iv 0 -out usbA/clefA &> /dev/null )

echo "Entrez le mot de passe du responsable B (mdpB): "
read -s mdpB
MDPB=$(echo $mdpB | hexdump -ve '1/1 "%02X"')
echo $CLEFB | (openssl enc -aes-128-cbc -pbkdf2 -K $MDPB -iv 0 -out usbB/clefB &> /dev/null )

echo "Initialisation terminée, les clefs ont été générées et le fichier sensible est à présent encrypté"




#!/bin/bash
echo "REENCRYPTION DE LA MAP BANCAIRE"
CLEF=$(cat clef)
openssl enc -aes-128-cbc -K $(echo $CLEF | hexdump -ve '1/1 "%02X"') -iv 0 -in mapBancaire -out mapBancaire.enc 

echo "SUPPRESSION DE LA MAP BANCAIRE EN CLAIR"
rm clef
rm mapBancaire

#!/bin/bash

echo "DESENCRYPTION DE LA MAP BANCAIRE, SOYEZ PRUDENT"
#On génère les clefs usb qui permettront de reformer la clef finale
echo "Etes vous le responsable A (oui/non)"
read responsableA

if [ $responsableA = "oui" ]
then
	echo "Entrez le mot de passe du responsable A (mdpA1): "
	read -s mdpA1
	MDPA1=$(echo $mdpA1 | hexdump -ve '1/1 "%02X"')
	CLEFA1=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPA1 -iv 0 -in usbA1/clefA1 )
else
	echo "Entrez le mot de passe du représentant légal A (mdpA2): "
	read -s mdpA2
	MDPA2=$(echo $mdpA2 | hexdump -ve '1/1 "%02X"')
	CLEFA2=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPA2 -iv 0 -in usbA2/clefA2 )
fi


echo "Etes vous le responsable B (oui/non)"
read responsableB

if [ $responsableB = "oui" ]
then
	echo "Entrez le mot de passe du responsable B (mdpB1): "
	read -s mdpB1
	MDPB1=$(echo $mdpB1 | hexdump -ve '1/1 "%02X"')
	CLEFB1=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPB1 -iv 0 -in usbB1/clefB1 )
else
	echo "Entrez le mot de passe du représentant légal B (mdpB2): "
	read -s mdpB2
	MDPB2=$(echo $mdpB2 | hexdump -ve '1/1 "%02X"')
	CLEFB2=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPB2 -iv 0 -in usbB2/clefB2 )
fi


if [ $responsableA = "oui" ] 
then
	if [ $responsableB = "oui" ]
	then
		CLEFA1B1=$((0x$CLEFA1 ^ 0x$CLEFB1))
		CLEF=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $(echo $CLEFA1B1 | hexdump -ve '1/1 "%02X"') -iv 0 -in disk/clefA1B1)
	else
		CLEFA1B2=$((0x$CLEFA1 ^ 0x$CLEFB2))
		CLEF=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $(echo $CLEFA1B2 | hexdump -ve '1/1 "%02X"') -iv 0 -in disk/clefA1B2)
	fi
else
	if [ $responsableB = "oui" ]
	then
		CLEFA2B1=$((0x$CLEFA2 ^ 0x$CLEFB1))
		CLEF=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $(echo $CLEFA2B1 | hexdump -ve '1/1 "%02X"') -iv 0 -in disk/clefA2B1)
	else
		CLEFA2B2=$((0x$CLEFA2 ^ 0x$CLEFB2))
		CLEF=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $(echo $CLEFA2B2 | hexdump -ve '1/1 "%02X"') -iv 0 -in disk/clefA2B2)
	fi
fi

echo $CLEF > disk/clef
openssl enc -d -aes-128-cbc -pbkdf2 -K $CLEF -iv 0 -in mapBancaire.enc -out mapBancaire

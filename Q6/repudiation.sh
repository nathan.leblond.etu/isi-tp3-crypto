#!/bin/bash

echo "Le membre à répudier est-il dans l'équipe A ou dans l'équipe B ?(A/B)"
read equipe

echo "Le membre de l'équipe $equipe à répudier est-il le responsable ou le suppléant ?(R/S)"
read role

echo "Repudiation du $role de l'équipe $equipe, êtes-vous sûrs ?(oui/non)"
read decision

if [ $decision = "oui" ]
then
	if [ $equipe = "A" ]
	then
		if [ $role = "R" ]
		then
			if [ -f "usbA2/clefA2" ]
			then
				echo "Entrez le mot de passe du supléant A (mdpA2): "
				read -s mdpA2
				MDPA2=$(echo $mdpA2 | hexdump -ve '1/1 "%02X"')
				CLEFA2=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPA2 -iv 0 -in usbA2/clefA2) || (echo "Mauvais mot de passe, fin de l'opération" && exit 1)
			fi
			if [ -f "usbB1/clefB1" ]
			then
				echo "Entrez le mot de passe du responsable B (mdpB1): "
				read -s mdpB1
				MDPB1=$(echo $mdpB1 | hexdump -ve '1/1 "%02X"')
				CLEFB1=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPB1 -iv 0 -in usbB1/clefB1) || (echo "Mauvais mot de passe, fin de l'opération" && exit 1)
			fi
			if [ -f "usbB2/clefB2" ]
			then
				echo "Entrez le mot de passe du supléant B (mdpB2): "
				read -s mdpB2
				MDPB2=$(echo $mdpB2 | hexdump -ve '1/1 "%02X"')
				CLEFB2=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPB2 -iv 0 -in usbB2/clefB2)|| (echo "Mauvais mot de passe, fin de l'opération" && exit 1)
			fi
			if [ -v CLEFA2 ] && [ -v CLEFB1 ] && [ -v CLEFB2 ]
			then
				rm -rf usbA1 disk/clefA1*
				echo "$role de l'équipe $equipe répudié"
			fi
		else
			if [ -f "usbA1/clefA1" ]
			then
				echo "Entrez le mot de passe du responsable A (mdpA1): "
				read -s mdpA1
				MDPA1=$(echo $mdpA1 | hexdump -ve '1/1 "%02X"')
				CLEFA1=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPA1 -iv 0 -in usbA1/clefA1) || (echo "Mauvais mot de passe, fin de l'opération" && exit 1)
			fi
			if [ -f "usbB1/clefB1" ]
			then
				echo "Entrez le mot de passe du responsable B (mdpB1): "
				read -s mdpB1
				MDPB1=$(echo $mdpB1 | hexdump -ve '1/1 "%02X"')
				CLEFB1=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPB1 -iv 0 -in usbB1/clefB1) || (echo "Mauvais mot de passe, fin de l'opération" && exit 1)
			fi
			if [ -f "usbB2/clefB2" ]
			then
				echo "Entrez le mot de passe du supléant B (mdpB2): "
				read -s mdpB2
				MDPB2=$(echo $mdpB2 | hexdump -ve '1/1 "%02X"')
				CLEFB2=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPB2 -iv 0 -in usbB2/clefB2) || (echo "Mauvais mot de passe, fin de l'opération" && exit 1)
			fi
			if [ -v CLEFA1 ] && [ -v CLEFB1 ] && [ -v CLEFB2 ]
			then
				rm -rf usbA2 disk/clefA2*
				echo "$role de l'équipe $equipe répudié"
			fi
		fi
	else
		if [ $role = "R" ]
		then
			if [ -f "usbA1/clefA1" ]
			then
				echo "Entrez le mot de passe du responsable A (mdpA1): "
				read -s mdpA1
				MDPA1=$(echo $mdpA1 | hexdump -ve '1/1 "%02X"')
				CLEFA1=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPA1 -iv 0 -in usbA1/clefA1) || (echo "Mauvais mot de passe, fin de l'opération" && exit 1)
			fi
			if [ -f "usbA2/clefA2" ]
			then
				echo "Entrez le mot de passe du supléant A (mdpA2): "
				read -s mdpA2
				MDPA2=$(echo $mdpA2 | hexdump -ve '1/1 "%02X"')
				CLEFA2=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPA2 -iv 0 -in usbA2/clefA2) || (echo "Mauvais mot de passe, fin de l'opération" && exit 1)
			fi
			if [ -f "usbB2/clefB2" ]
			then
				echo "Entrez le mot de passe du supléant B (mdpB2): "
				read -s mdpB2
				MDPB2=$(echo $mdpB2 | hexdump -ve '1/1 "%02X"')
				CLEFB2=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPB2 -iv 0 -in usbB2/clefB2) || (echo "Mauvais mot de passe, fin de l'opération" && exit 1)
			fi
			if [ -v CLEFA1 ] && [ -v CLEFA2 ] && [ -v CLEFB2 ]
			then
				rm -rf usbB1 disk/clef*B1
				echo "$role de l'équipe $equipe répudié"
			fi
		else
			if [ -f "usbA1/clefA1" ]
			then
				echo "Entrez le mot de passe du responsable A (mdpA1): "
				read -s mdpA1
				MDPA1=$(echo $mdpA1 | hexdump -ve '1/1 "%02X"')
				CLEFA1=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPA1 -iv 0 -in usbA1/clefA1) || (echo "Mauvais mot de passe, fin de l'opération" && exit 1)
			fi
			if [ -f "usbA2/clefA2" ]
			then
				echo "Entrez le mot de passe du supléant A (mdpA2): "
				read -s mdpA2
				MDPA2=$(echo $mdpA2 | hexdump -ve '1/1 "%02X"')
				CLEFA2=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPA2 -iv 0 -in usbA2/clefA2) || (echo "Mauvais mot de passe, fin de l'opération" && exit 1)
			fi
			if [ -f "usbB1/clefB1" ]
			then
				echo "Entrez le mot de passe du responsable B (mdpB1): "
				read -s mdpB1
				MDPB1=$(echo $mdpB1 | hexdump -ve '1/1 "%02X"')
				CLEFB1=$(openssl enc -d -aes-128-cbc -pbkdf2 -K $MDPB1 -iv 0 -in usbB1/clefB1) || (echo "Mauvais mot de passe, fin de l'opération" && exit 1)
			fi
			if [ -v CLEFA1 ] && [ -v CLEFA2 ] && [ -v CLEFB1 ]
			then
				rm -rf usbB2 disk/clef*B2
				echo "$role de l'équipe $equipe répudié"
			fi
		fi
	fi
else
	exit 1
fi

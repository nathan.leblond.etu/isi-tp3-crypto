#!/bin/bash
echo "REENCRYPTION DE LA MAP BANCAIRE"
CLEF=$(cat disk/clef)
openssl enc -aes-128-cbc -pbkdf2 -K $CLEF -iv 0 -in mapBancaire -out mapBancaire.enc

echo "SUPPRESSION DE LA MAP BANCAIRE EN CLAIR"
#rm disk/clef
rm mapBancaire

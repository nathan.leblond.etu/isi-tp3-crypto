#!/bin/bash

PWD=$(pwd)
ALGO=16
echo "Initialisation des clefs usb"

# On initialise les dossiers/fichiers nécessaires
mkdir usb{A1,B1,A2,B2} disk
touch mapBancaire

#On génère des clefs aléatoire à partir des carac. physiques de la machine à l'initialisation
CLEF=$(dd if=/dev/random bs=$ALGO count=1 | hexdump -ve '1/1 "%02X"')

#On génère des clefs aléatoire à partir des carac. physiques de la machine à l'initialisation
CLEFA1=$(dd if=/dev/random bs=$ALGO count=1 | hexdump -ve '1/1 "%02X"')
CLEFB1=$(dd if=/dev/random bs=$ALGO count=1 | hexdump -ve '1/1 "%02X"')
CLEFA2=$(dd if=/dev/random bs=$ALGO count=1 | hexdump -ve '1/1 "%02X"')
CLEFB2=$(dd if=/dev/random bs=$ALGO count=1 | hexdump -ve '1/1 "%02X"')

# On forme la clef finale qui servira pour le fichier à protéger, on l'encode avec puis on supprime la version claire
CLEFA1B1=$((0x$CLEFA1 ^ 0x$CLEFB1))
CLEFA1B2=$((0x$CLEFA1 ^ 0x$CLEFB2))
CLEFA2B1=$((0x$CLEFA2 ^ 0x$CLEFB1))
CLEFA2B2=$((0x$CLEFA2 ^ 0x$CLEFB2))
openssl enc -aes-128-cbc -pbkdf2 -K $CLEF  -in mapBancaire -iv 0 -out mapBancaire.enc  
echo $CLEF | openssl enc -aes-128-cbc -pbkdf2 -K $(echo $CLEFA1B1 | hexdump -ve '1/1 "%02X"') -iv 0 -out disk/clefA1B1  
echo $CLEF | openssl enc -aes-128-cbc -pbkdf2 -K $(echo $CLEFA1B2 | hexdump -ve '1/1 "%02X"') -iv 0 -out disk/clefA1B2  
echo $CLEF | openssl enc -aes-128-cbc -pbkdf2 -K $(echo $CLEFA2B1 | hexdump -ve '1/1 "%02X"') -iv 0 -out disk/clefA2B1  
echo $CLEF | openssl enc -aes-128-cbc -pbkdf2 -K $(echo $CLEFA2B2 | hexdump -ve '1/1 "%02X"') -iv 0 -out disk/clefA2B2  

rm mapBancaire

#On génère les clefs usb qui permettront de reformer la clef finale 
echo "Entrez le mot de passe du responsable A (mdpA1): "
read -s mdpA1
MDPA1=$(echo $mdpA1 | hexdump -ve '1/1 "%02X"')
echo $CLEFA1 | (openssl enc -aes-128-cbc -pbkdf2 -K $MDPA1 -iv 0 -out usbA1/clefA1 )

echo "Entrez le mot de passe du responsable B (mdpB1): "
read -s mdpB1
MDPB1=$(echo $mdpB1 | hexdump -ve '1/1 "%02X"')
echo $CLEFB1 | (openssl enc -aes-128-cbc -pbkdf2 -K $MDPB1 -iv 0 -out usbB1/clefB1 )


echo "Entrez le mot de passe du représentant légal A (mdpA2): "
read -s mdpA2
while [ $mdpA2 = $mdpA1 ]
do 
	echo "Veuillez adopter un mot de passe différent"
	read -s mdpA2
done
MDPA2=$(echo $mdpA2 | hexdump -ve '1/1 "%02X"')
echo $CLEFA2 | (openssl enc -aes-128-cbc -pbkdf2 -K $MDPA2 -iv 0 -out usbA2/clefA2 )

echo "Entrez le mot de passe du représentant légal B (mdpB2): "
read -s mdpB2
while [ $mdpB2 = $mdpB1 ]
do 
	echo "Veuillez adopter un mot de passe différent"
	read -s mdpB2
done
MDPB2=$(echo $mdpB2 | hexdump -ve '1/1 "%02X"')
echo $CLEFB2 | (openssl enc -aes-128-cbc -pbkdf2 -K $MDPB2 -iv 0 -out usbB2/clefB2 )

echo "Initialisation terminée, les clefs ont été générées et le fichier sensible est à présent encrypté"



